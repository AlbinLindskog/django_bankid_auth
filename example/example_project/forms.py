from django import forms
from django_bankid_auth import SignForm as BankIDSignForm, AuthenticationForm as BankIDAuthenticationForm

from .models import CustomUser


class SignForm(BankIDSignForm):

    def __init__(self, request, *args, **kwargs):
        super(SignForm, self).__init__(request, *args, **kwargs)
        self.fields['count'] = forms.IntegerField(label='Number of objects')

    def get_personal_number(self):
        return self.request.user.personal_number

    def get_message(self):
        return 'Hej'

    def clean_count(self):
        data = self.cleaned_data['count']
        if data % 2:
            raise forms.ValidationError('{} is not an even number'.format(data))
        if data < 1:
            raise forms.ValidationError('{} is not larger then 0'.format(data))

        return data

class AuthenticationForm(BankIDAuthenticationForm):

    def get_user(self):
        personal_number = self.cleaned_data['personal_number']
        return CustomUser.objects.get(personal_number=personal_number)
