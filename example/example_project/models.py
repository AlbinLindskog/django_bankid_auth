from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    last_sign = models.DateTimeField(blank=True, null=True)
    personal_number = models.BigIntegerField(unique=True)

    REQUIRED_FIELDS = ['email', 'personal_number']


class Order(models.Model):
    pass
