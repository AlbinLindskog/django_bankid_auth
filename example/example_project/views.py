from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LogoutView, LoginView as DjangoLoginView
from django.views.generic.base import TemplateView

from django_bankid_auth import BankIDFormView

from .models import Order
from .forms import SignForm, AuthenticationForm


class HomePageView(TemplateView):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['order_count'] = Order.objects.count()
        context['user_logged_in'] = self.request.user.is_authenticated()
        return context


class AddOrderView(LoginRequiredMixin, BankIDFormView):

    template_name = "addorder.html"
    form_class = SignForm
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super(AddOrderView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        count = form.cleaned_data['count']
        for _ in range(count):
            Order.objects.create()
        return super(AddOrderView, self).form_valid(form)


class LoginView(DjangoLoginView, BankIDFormView):
    template_name = 'login.html'
    form_class = AuthenticationForm


add_order_view = AddOrderView.as_view()
homepage_view = HomePageView.as_view()
login_view = LoginView.as_view()
logout_view = LogoutView.as_view()