from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from .views import login_view, logout_view, homepage_view, add_order_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^order/', add_order_view, name='add_order'),
    url(r'^login/', login_view, name='login'),
    url(r'^logout/', logout_view, name='logout'),
    url(r'^$', homepage_view, name='home_page'),
]