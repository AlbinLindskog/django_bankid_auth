The goal of this project is to allow you as a developer to incorporate the use of BankID for authentication/signing
into your Django application as unobtrusively as possible. Simply use django_bankid_auth's BankIDFormView as you would
any other FormView. The only caveat is that your forms must subclass django_bankid_auth's AuthenticationForm and
SignForm respectively.


Installation
============
Install the package,::

    pip3 install git+https://bitbucket.org/AlbinLindskog/django_bankid_auth

include it in your installed apps,::

    INSTALLED_APPS = [
            ...
            'django_bankid_auth',
            ...
        ]

and point django-bankid-auth towards your certificate files using the BANKID_CERT_PATH and BANKID_KEY_PATH settings.::

    BANKID_TEST_SERVER = True
    BANKID_CERT_PATH = 'path/to/my/cert.pem'
    BANKID_KEY_PATH = 'path/to/my/key.pem'

BANKID_TEST_SERVER denotes whether the certificates are for the BankID test-environment.


Templates
=========
Django-bankid-auth relies on Javascript and JQuery to function. As such you must make sure to include {{ form.media }},
in your template. If you do not use JQuery in the rest of your project you must include it as well. Add the following
to your template::

    {% block extra_head %}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        {{ form.media }}
    {% endblock %}

If you do not include these files, or if the user have disabled Javascript, any user will be prompted by an error
message urging them to enable Javascript.

Additionally in order for the Javascript hook up properly, the class of the form must be "bankid-form", and include a
csrf-token. An example of a login-template is given below.::

    <form class="bankid-form" id="login-form" method="POST" action="{% url 'login' %}">
        {% csrf_token %}
        {{ form }}
      <button id="bankIDlogin">{% trans "Sign In" %}</button>
    </form>


Customizable messages
=====================
When the user clicks sign/login a message is displayed asking them to start their BankID-application. To customize this
message simply implement your own bankid_message.html template. The default template looks as following::

    <div id="start_bankid_message" style="display: none;">
        <div id="bankid_status">{% trans "Start your BankID app." %}</div>
        <div id="bankid_cancel">{% trans "cancel" %}</div>
    </div>

Note that in order for the Javascript to function properly must the message be an element with id="start_bankid_message",
the cancel button an element with id="bankid_cancel".

If you wish to customize this message further than what is possible using a static method, simply extend the
Authentication and/or SignForm and extend the forms render_message-method, herein you can provide additional context
for the message.


Testing
=======
To help with testing django_bankid_auth comes with a MockBankIDClient that makes no external calls. Simply import it
from django_bankid_auth.tests and mock the regular BankIDClient per::

    from django.test import TestCase

    from django_bankid_auth.tests import mock_bankid_client


    @patch('django_bankid_auth.views.BankIDClient', mock_bankid_client())
    def test_something(self, *args):
        response = self.client.post(test-url,
                                    {'test_data': 'TestData',
                                     'order_ref': 'TestTest-Test-Test-Test-TestTestTest',
                                     },
                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        assertSomething(response)


Note that you need to specify that it is a ajax request. If you are using Django's TestClient it is done using
HTTP_X_REQUESTED_WITH='XMLHttpRequest', as above.

If you wish to test the view as if the user have signed/authenticated themselves you need to provided a correctly
formatted order reference, for example 'TestTest-Test-Test-Test-TestTestTest'. If you wish to test the view as if the
user places an sign/authentication order simply do not post a order_ref.

The default behaviour of MockBankIDClient is as if all went well, i.e. sign and authentication calls return an order
reference, collect returns a signed order. If you wish to changed this behaviour you can specify the desired return
values of the three methods using the authenticate, sign and collect arguments respectivly. E.g::

    from django_bankid_auth.tests import mock_bankid_client


    @patch('django_bankid_auth.views.BankIDClient', mock_bankid_client(collect={'progressStatus': 'INCOMPLETE'}))
        def test_something_else(self, *args):
            ...


Example project
===============
A bare-bones project is included. To start it simply run::

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver


The username and password you provide are used to login in the admin-suite. The personal_number you provide is used
throughout the rest of the example project. Note that you will need the test bankID application. More information
`here <https://www.bankid.com/assets/bankid/rp/how-to-get-bankid-for-test-v1.5.pdf/>`_.

The example project also includes some examples one how to write tests as discussed in the section above.


TODO
====
Cleaner JS integration.
Pretty up example project with CSS.
Continuously update the status of the order.
Option to automatically attempt to launch the BankID application.
