from distutils.core import setup
from setuptools import find_packages

setup(
    name='Django BankID Auth',
    version='0.0.1',
    long_description=open('README.rst').read(),
    install_requires=['Django >= 1.8', 'pybankid >= 0.3.0', 'django-localflavor >= 1.3'],
    packages=find_packages(exclude=['example']),
    author='Albin Lindskog',
    author_email='albin@zerebra.com',
    url='https://bitbucket.org/AlbinLindskog/django_bankid_auth/src',
    include_package_data=True,
    zip_safe=False,
)