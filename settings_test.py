"""
Django settings required to run the test_suite for django_bankid_auth
"""
import os

SECRET_KEY = 'Supersecret'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django_bankid_auth',
    'django_nose',
]

MIDDLEWARE = []

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--with-coverage',
    '--cover-html',
    '--cover-package=django_bankid_auth',
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'django_bankid_auth/django_bankid_auth/tests/templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'dummy',
    }
}

STATIC_ROOT = '/tmp/'  # Dummy
STATIC_URL = '/static/'
ROOT_URLCONF = 'django_bankid_auth.tests.urls'

# django-bankid_auth settings:
BANKID_TEST_SERVER = True
BANKID_CERT_PATH = '/'
BANKID_KEY_PATH = '/'
