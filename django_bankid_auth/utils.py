# -*- coding: utf-8 -*-

from django.forms import TextInput
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _

from bankid.exceptions import BankIDError
from localflavor.se.forms import SEPersonalIdentityNumberField


default_error = _('Unexpected error. Please try again.')


def get_error_message(rfa):
    """
    Returns error messages specified by BankId.

    rfa is the error message identifier used by BankId.
    """
    error_messages = {3: _('Action cancelled. Please try again.'),
                      5: _('Internal error. Please try again.'),
                      6: _('Action cancelled.'),
                      8: _('The BankID app is not responding. Please check that the program is started and that you '
                           'have internet access. If you don’t have a valid BankID you can get one from your bank. '
                           'Try again.'),
                      12: _('Internal error. Update your BankID app and try again.'),
                      17: _('The BankID app couldn’t be found on your computer or mobile device. Please install it and '
                            'order a BankID from your internet bank. Install the app from install.bankid.com.'),
                      }
    return str(error_messages.get(rfa, default_error))


def get_status_message(progress_status):
    """
    Returns status messages specified by BankId.

    progress_status is the identifier used by BankId.
    """
    status_messages = {'OUTSTANDING_TRANSACTION': _('Trying to start your BankID app.'),
                       'NO_CLIENT': _('Start your BankID app.'),
                       'STARTED': _('Searching for BankID:s, it may take a little while… \n\n If a few seconds have '
                                    'passed and still no BankID has been found, you probably don’t have a BankID which '
                                    'can be used for this login/signature on this device. If you don’t have a BankID '
                                    'you can order one from your internet bank. If you have a BankID on another device '
                                    'you can start the BankID app on that device.'),
                       'USER_SIGN': 'Enter your security code in the BankID app and select Identify or Sign.',
                       }

    return str(status_messages.get(progress_status))


class PersonalNumberField(SEPersonalIdentityNumberField):

    def __init__(self, *args, **kwargs):
        self.widget = TextInput(attrs={'type': 'text',
                                       'placeholder': _('Personal number'),
                                       'autofocus': 'autofocus'})
        super(PersonalNumberField, self).__init__(self, *args, **kwargs)


def bankid_exc_handle(method):
    """
    Decorator for handling the potential Exceptions raised when calling the BankID-server.
    """
    def exc_handle(*args, **kwargs):
        try:
            return method(*args, **kwargs)
        except BankIDError as e:
            return JsonResponse({'errors': {'__all__': [get_error_message(e.rfa), ]}})
        except Exception as e:
            return JsonResponse({'errors': {'__all__': [get_error_message(-1), ]}})  # Fallback

    return exc_handle