from django import forms
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse  # Pre django 1.10 support

from .utils import PersonalNumberField, default_error


class _BaseBankIDForm(forms.Form):
    message_template = 'django_bankid_auth/bankid_message.html'

    fallback_error = forms.CharField(widget=forms.HiddenInput(), required=False, initial=default_error)
    order_ref = forms.RegexField(required=False, widget=forms.HiddenInput(),
                                 regex=r'^[A-Za-z0-9]{8}\-[A-Za-z0-9]{4}\-[A-Za-z0-9]{4}\-[A-Za-z0-9]{4}\-[A-Za-z0-9]{12}$')

    class Media:
        js = ('django_bankid_auth/js/bank_id_base.min.js',)

    def __init__(self, request, *args, **kwargs):
        super(_BaseBankIDForm, self).__init__(*args, **kwargs)
        self.request = request
        self.message = self.render_message()
        self.user_info = None
        self.signature = None
        self.status = None

    def clean(self):
        """
        Clean the form. If the form is invalid because it was not submitted with ajax only that error-message is 
        displayed regardless of other errors.
        """
        super(_BaseBankIDForm, self).clean()
        if not self.request.is_ajax():
            self._errors.clear()
            raise forms.ValidationError(_('Please enable Javascript to use BankID.'))

        return self.cleaned_data

    def render_message(self):
        context = {}
        message = render_to_string(self.message_template, context)
        return message

    def parse_status(self, status):
        self.status = status['progressStatus']
        if self.status == 'COMPLETE':
            self.user_info = status['userInfo']
            self.signature = status['signature']

    @property
    def signed(self):
        return bool(self.signature)

    def _html_output(self, normal_row, error_row, row_ender, help_text_html, errors_on_separate_row):
        """
        Attach the rendered message as a sibling to the rendered form, so {{ form }} renders everything.
        """
        html = super(_BaseBankIDForm, self)._html_output(normal_row, error_row, row_ender,
                                                         help_text_html, errors_on_separate_row)
        return html + self.message


class AuthenticationForm(_BaseBankIDForm):
    """
    Login-form using BankId.
    """

    def __init__(self, request, *args, **kwargs):
        super(AuthenticationForm, self).__init__(request, *args, **kwargs)
        self.fields['personal_number'] = PersonalNumberField(label=_('Personal number:'), required=True)

    def confirm_login_allowed(self, user):
        raise NotImplementedError('Subclasses of AuthenticationForm must provide a confirm_login_allowed() method.')

    def get_user(self):
        raise NotImplementedError('Subclasses of AuthenticationForm must provide a get_user() method.')


class SignForm(_BaseBankIDForm):

    def get_personal_number(self):
        raise NotImplementedError('Subclasses of SignForm must provide a get_personal_number() method.')

    def get_message(self):
        raise NotImplementedError('Subclasses of SignForm must provide a get_message() method.')
