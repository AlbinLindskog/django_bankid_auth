��          �            x     y  #   �  !   �  5   �            '   (  s  P  "   �     �  �   �  �   �      g  #   �     �  e  �       #   +  !   O  5   q     �     �  '   �  q  �  "   b	     �	  �   �	  �   I
        #   &     J                  
              	                                               Action cancelled. Action cancelled. Please try again. Internal error. Please try again. Internal error. Update your BankID app and try again. Personal number Personal number: Please enable Javascript to use BankID. Searching for BankID:s, it may take a little while… 

 If a few seconds have passed and still no BankID has been found, you probably don’t have a BankID which can be used for this login/signature on this device. If you don’t have a BankID you can order one from your internet bank. If you have a BankID on another device you can start the BankID app on that device. Start MobileBankID on this Device. Start your BankID app. The BankID app couldn’t be found on your computer or mobile device. Please install it and order a BankID from your internet bank. Install the app from install.bankid.com. The BankID app is not responding. Please check that the program is started and that you have internet access. If you don’t have a valid BankID you can get one from your bank. Try again. Trying to start your BankID app. Unexpected error. Please try again. cancel Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-04 10:09-0500
PO-Revision-Date: 2017-09-04 10:11-0551
Last-Translator: b'  <test@mail.com>'
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Translated-Using: django-rosetta 0.7.13
 Action cancelled. Action cancelled. Please try again. Internal error. Please try again. Internal error. Update your BankID app and try again. Personal number Personal number: Please enable JavaScript to use BankID. Searching for BankID:s, it may take a little while… 

If a few seconds have passed and still no BankID has been found, you probably don’t have a BankID which can be used for this login/signature on this device. If you don’t have a BankID you can order one from your internet bank. If you have a BankID on another device you can start the BankID app on that device Start MobileBankID on this Device. Start your BankID app. The BankID app couldn’t be found on your computer or mobile device. Please install it and order a BankID from your internet bank. Install the app from install.bankid.com. The BankID app is not responding. Please check that the program is started and that you have internet access. If you don’t have a valid BankID you can get one from your bank. Try again. Trying to start your BankID app. Unexpected error. Please try again. cancel 