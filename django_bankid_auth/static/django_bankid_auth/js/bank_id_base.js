/**
 * Created by albinlindskog on 2017-07-31.
 */

window.onload = function(e) {
    setUpJQuery();
    form = $('.bankid-form');
    form.on('submit', function(event){
        event.preventDefault();
        initiate_order();
    });
};

function setUpJQuery() {
    var csrftoken = $('input[name="csrfmiddlewaretoken"]').val();
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });
}

function initiate_order() {
    // Posts a sign/authentication requests to the server. If the form is valid it receives an order-reference used to
    // check the status of the request. If the form is invalid it renders the form with the error message.
    var form = $('.bankid-form');
    $('.errorlist').remove();

    $.ajax({
        method: 'POST',
        url: form.attr('action'),
        data: form.serialize()
    }).done(function (response) {
        if ('errors' in response) {
            clearForm();
            addErrors(response['errors']);
            showForm();
        } else if ('orderRef' in response) {
            poll(response['orderRef']);
        }
    }).fail(function () {
        addFallbackError();
        showForm();
    })
}

function poll(orderRef) {
    // Set up calls to server every 2 secs checking the status of the order. Set upp event-handling to cancel order.
    $('input[name="order_ref"]').val(orderRef);
    hideForm();

    interval = 0;
    var interval = setInterval(function(){
        collect_order(interval);
    }, 2000);

    $('#bankid_cancel').click(function() {
        cancelCollect(interval);
        clearForm();
        showForm();
    });
}

function cancelCollect(interval) {
    clearInterval(interval);
}

function collect_order(interval) {
    // Makes an request and checks if the order have been signed. If the order have been signed the form is posted for
    // back-end validation and handling. If the order have not been signed it does nothing and poll() keeps checking.
    var form = $('.bankid-form');

    $.ajax({
        method: 'POST',
        url: form.attr('action'),
        data: form.serialize()
    }).done(function (response, status, xhr) {
        if ('errors' in response) {
            cancelCollect(interval);
            clearForm();
            addErrors(response['errors']);
            showForm();
        } else if ('redirect_url' in response) {
            window.location.href = response['redirect_url']  // Signed. redirect
        } else {
            // Not signed. Do nothing. Keep polling.
        }
    }).fail(function () {
        cancelCollect(interval);
        clearForm();
        addFallbackError();
        showForm();
    })
}

function addErrors(error_dict) {
    // Clears all errors and adds error to a field following the style of Django
    $('.errorlist').remove();
    for (var field in error_dict) {
        var errors = error_dict[field];
        if (field === '__all__') {
            addFormErrors(errors)
        } else {
            addFieldErrors(errors, field)
        }
    }
}

function addFieldErrors(errors, field) {
    // Adds field level errors in f the style of Django.
    var label = $('label[for=id_' + field + ']');
    $('<ul class="errorlist"></ul>').insertAfter(label);
    for (var i = 0, size = errors.length; i < size; i++) {
        var error = errors[i];
        $('.errorlist').append('<li>' + error + '</li>');
    }
}

function addFormErrors(errors) {
    // Adds form level errors in the style of Django.
    var form = $('.bankid-form');
    form.prepend('<ul class="errorlist"></ul>');
    for(var i = 0, size = errors.length; i < size ; i++){
        var error = errors[i];
        $('.errorlist').append('<li>' + error + '</li>');
    }
}

function addFallbackError() {
    // Clears all previous errors and adds error to the form following the style of Django.
    var fallback_error = $('#id_fallback_error')[0].value;
    addErrors({'__all__': [fallback_error]})
}

function clearForm() {
    // Clears the form so the user can try again.
    $('input[name="order_ref"]').val('');
}

function hideForm() {
    // Hides all elements in the form, except the start_bankid_message.
    var message = $('#start_bankid_message');
    message.siblings().hide();
    message.show();
}

function showForm() {
    // Hides the start_bankid_message and instead shows all elements in the form.
    var message = $('#start_bankid_message');
    message.siblings().show();
    message.hide();
}