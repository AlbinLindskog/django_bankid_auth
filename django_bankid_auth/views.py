from bankid import BankIDClient

from django.conf import settings
from django.http import JsonResponse
from django.views.generic.edit import FormView

from .utils import get_status_message, bankid_exc_handle
from .forms import AuthenticationForm, SignForm


class BankIDFormView(FormView):

    def __init__(self, *args, **kwargs):
        super(BankIDFormView, self).__init__(*args, **kwargs)
        self.client = None

    def init_client(self):
        """
        Initialise the bankid-client.
         
        This is kept in a separate method instead of __init__ too avoid make external request until 
        it is necessary, e.g. not when handling get-requests.
        """
        self.client = BankIDClient(certificates=(settings.BANKID_CERT_PATH,
                                                 settings.BANKID_KEY_PATH),
                                   test_server=settings.BANKID_TEST_SERVER)

    def get_form(self, form_class=None):
        """
        Return an instance of the form to be used in this view. Must be a subclass of either 
        AuthenticateForm or SignForm.
        """
        if form_class is None:
            form_class = self.get_form_class()

        if not issubclass(form_class, (AuthenticationForm, SignForm)):
            raise ValueError('Form must be a subclass of either SignForm or AuthenticateForm.')

        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(BankIDFormView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.manage_orders(form)
        else:
            return self.form_invalid(form)

    def form_invalid(self, form):
        """
        If the form is invalid because it was not submitted using ajax render a complete html response, otherwise
        serialize the form errors and let the client side JS render them.
        """
        if not self.request.is_ajax():
            return self.render_to_response(self.get_context_data(form=form))
        else:
            return JsonResponse({'errors': dict(form.errors.items())})

    def manage_orders(self, form):
        self.init_client()

        if form.cleaned_data['order_ref']:
            return self.collect_order(form)

        if isinstance(form, SignForm):
            return self.place_sign_order(form)

        if isinstance(form, AuthenticationForm):
            return self.place_authenticate_order(form)

    @bankid_exc_handle
    def place_authenticate_order(self, form):
        personal_number = form.cleaned_data['personal_number']
        token = self.client.authenticate(personal_number=personal_number)
        return JsonResponse({'orderRef': token['orderRef']})

    @bankid_exc_handle
    def place_sign_order(self, form):
        personal_number = form.get_personal_number()
        message = form.get_message()
        token = self.client.sign(personal_number=personal_number, user_visible_data=message)
        return JsonResponse({'orderRef': token['orderRef']})

    @bankid_exc_handle
    def collect_order(self, form):
        order_ref = form.cleaned_data['order_ref']
        status = self.client.collect(order_ref=order_ref)
        form.parse_status(status)
        if form.signed:
            return self.completed_order(form)
        else:
            return JsonResponse({'status': get_status_message(form.status)})

    def completed_order(self, form):
        """
        Complete the order by handling the posted form as one would any other form and recast the HttpResponseRedirect 
        usually returned by form_valid() to a JSONResponse usable by the ajax that made the request.
        """
        res = self.form_valid(form)
        return JsonResponse({'redirect_url': res.url})
