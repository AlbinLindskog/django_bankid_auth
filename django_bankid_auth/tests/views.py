from django_bankid_auth.forms import SignForm, AuthenticationForm
from django_bankid_auth.views import BankIDFormView


class TestSignForm(SignForm):

    def get_personal_number(self):
        return '198405124523'

    def get_message(self):
        return 'Test'


class SignView(BankIDFormView):
    template_name = 'test_template.html'
    form_class = TestSignForm
    success_url = '/sign/'


class AuthenticateView(BankIDFormView):
    template_name = 'test_template.html'
    form_class = AuthenticationForm
    success_url = '/authenticate/'
