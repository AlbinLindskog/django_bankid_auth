from django.http import JsonResponse
from django.test import TestCase

from bankid.exceptions import AlreadyInProgressError

from django_bankid_auth.utils import bankid_exc_handle


class BankIDExecHandleTestCase(TestCase):
    """
    Unittests for the bankid_exc_handle-decorator.
    """

    def test_no_exec(self):
        """
        bankid_exc_handle should not alter the behaviour if no exception is raised.
        """
        @bankid_exc_handle
        def func(): return 2

        response = func()
        self.assertEqual(response, 2)

    def test_bankid_exec(self):
        """
        bankid_exc_handle should return a JSON-response with the corresponding error if a bankid-error is raised.
        """
        @bankid_exc_handle
        def func(): raise AlreadyInProgressError

        response = func()
        error = JsonResponse({'errors': {'__all__': ['Action cancelled. Please try again.', ]}})
        self.assertEqual(response.content, error.content)

    def test_exec(self):
        """
        bankid_exc_handle should return a JSON-response with the default error if a non-bankid-error is raised.
        """
        @bankid_exc_handle
        def func(): raise KeyError

        response = func()
        error = JsonResponse({'errors': {'__all__': ['Unexpected error. Please try again.', ]}})
        self.assertEqual(response.content, error.content)