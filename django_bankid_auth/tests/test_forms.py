from django.http import HttpRequest
from django.test import TestCase

from django_bankid_auth.forms import _BaseBankIDForm


class BaseBankIDFormTestCase(TestCase):
    """
    Unittests for the BaseBankIDForm
    """

    def test_message(self):
        """
        The message should be rendered and added as an instance variable as well as appended as a 
        sibling to the rendered form.
        """
        request = HttpRequest()
        form = _BaseBankIDForm(request)
        self.assertIsNotNone(form.message)

        html = form.as_table()
        message = '<div id="start_bankid_message" style="display: none;">'
        self.assertIn(message, html)

    def test_clean(self):
        """
        Clean should remove any other errors if the request was not made with ajax, and display the 
        appropriate error message, as it indicates that the javascript did not load correctly.
        """
        invalid_data = {'order_ref': 'invalid-dummy'}

        request = HttpRequest()
        form = _BaseBankIDForm(request, data=invalid_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            '__all__': ['Please enable JavaScript to use BankID.'],
        })

    def test_clean_ajax(self):
        """
        The error should be handled as expected if the request is made with ajax.
        """
        invalid_data = {'order_ref': 'invalid-dummy'}

        request = HttpRequest()
        request.META = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}  # ajax
        form = _BaseBankIDForm(request, data=invalid_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'order_ref': ['Enter a valid value.']})