from datetime import datetime


def mock_bankid_client(autenticate=None, sign=None, collect=None):
    """
    A replacement for the BankID-client for testing that makes no external requests.
    """
    class MockBankIDClient(object):

        def __init__(self, certificates, *args, **kwargs):
            self.authenticate_var = autenticate
            self.sign_var = sign
            self.collect_var = collect
            super(MockBankIDClient, self).__init__()

        def authenticate(self, personal_number, **kwargs):
            return self.authenticate_var or {'orderRef': 'TestTest-Test-Test-Test-TestTestTest'}

        def sign(self, user_visible_data, personal_number=None, **kwargs):
            return self.sign_var or {'orderRef': 'TestTest-Test-Test-Test-TestTestTest'}

        def collect(self, order_ref):
            return self.collect_var or {'progressStatus': 'COMPLETE',
                                        'userInfo': {'name': 'Albin Gunnar Lindskog',
                                                     'givenName': 'Albin Gunnar',
                                                     'surname': 'Lindskog',
                                                     'ipAddress': '190.19.120.120',
                                                     'notAfter': datetime(2020, 1, 1, 0, 0, 0),
                                                     'notBefore': datetime(2018, 1, 1, 0, 0, 0),
                                                     'personalNumber': '199407285775'},}

    return MockBankIDClient
