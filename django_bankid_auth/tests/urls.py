from django.conf.urls import url

from .views import SignView, AuthenticateView

urlpatterns = [
    url(r'^sign/', SignView.as_view(), name='sign'),
    url(r'^authenticate/', AuthenticateView.as_view(), name='authenticate'),
]