from mock import patch

from django import forms
from django.core.urlresolvers import reverse
from django.http import HttpRequest, JsonResponse
from django.test import Client, TestCase
from django.template.response import TemplateResponse

from django_bankid_auth.forms import SignForm, AuthenticationForm
from django_bankid_auth.tests import mock_bankid_client
from django_bankid_auth.views import BankIDFormView


class BankIDFormViewTestCase(TestCase):
    """
    Unittests for the BankIDFormView.
    """

    def setUp(self):
        self.client = Client()

    def test_get_form(self):
        """
        Should raise an exception if the form is not a subclass of SignForm or AuthenticationForm, else it 
        should return the form_class
        """
        request = HttpRequest()
        view = BankIDFormView(**{'request': request})
        view.form_class = SignForm

        form = view.get_form()
        self.assertIsInstance(form, SignForm)

        view.form_class = forms.Form
        with self.assertRaises(ValueError):
            form = view.get_form()

    def test_post_form_invalid(self):
        """
        The form_invalid method should return a complete HTML-page if the form is invalid because it was 
        not submitted with ajax. If it was submitted with ajax a json with only the errors should be returned.
        """
        invalid_data = {'order_ref': 'invalid-dummy'}

        # Not ajax:
        response = self.client.post(reverse('sign'), invalid_data)
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(response.template_name, ['test_template.html'])
        self.assertFormError(response, 'form', '__all__', ['Please enable JavaScript to use BankID.'])

        # ajax:
        response = self.client.post(reverse('sign'), invalid_data,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.content.decode(), '{"errors": {"order_ref": ["Enter a valid value."]}}')

    @patch('django_bankid_auth.views.BankIDClient', mock_bankid_client())
    def test_collect_signed(self):
        """
        If the request have been signed by bankid, form_valid should be called, and the return value should be 
        returned as JsonResponse, not a TemplateResponse. 
        """
        valid_data = {'order_ref': 'TestTest-Test-Test-Test-TestTestTest'}

        response = self.client.post(reverse('sign'), valid_data,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.content.decode(), '{"redirect_url": "/sign/"}')

    @patch('django_bankid_auth.views.BankIDClient', mock_bankid_client(collect={'progressStatus': 'NO_CLIENT'}))
    def test_collect_not_signed(self):
        """
        If the request have not been signed by bankid, a JSON containing the status of the order should be returned.
        """
        valid_data = {'order_ref': 'TestTest-Test-Test-Test-TestTestTest'}

        response = self.client.post(reverse('sign'), valid_data,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.content.decode(), '{"status": "Start your BankID app."}')

    @patch('django_bankid_auth.views.BankIDClient', mock_bankid_client())
    def test_init_order(self):
        """
        Initialising an order should return a JsonResponse of the order-ref.
        """
        response = self.client.post(reverse('sign'), {},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.content.decode(), '{"orderRef": "TestTest-Test-Test-Test-TestTestTest"}')

        response = self.client.post(reverse('authenticate'),
                                    {'personal_number': '195408274024'},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.content.decode(), '{"orderRef": "TestTest-Test-Test-Test-TestTestTest"}')