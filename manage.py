#!/usr/bin/env python
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings_test'
from django.core import management
if __name__ == "__main__":
    management.execute_from_command_line()